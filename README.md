# Project install

## Global Setup
- Instlall nvm (https://github.com/nvm-sh/nvm).
- Install Installing Cloud SDK (https://cloud.google.com/sdk/docs/install).

Run `nvm install xx.xx.x`
Run `nvm use xx.xx.x`
Run `npm i -g yarn`
Run `npm i -g @angular/cli`
Run `npm i -g @nestjs/cli`
Run `yarn add -D lerna` (`npm i -g lerna`)
Run `yarn add -D commitizen` (`npm i -g commitizen`)
Run `yarn install`
Run `npm install -g npm-check-updates`


# How The Project Was Setup
## yarn
1. yarn is used because npm workspace does not support nohoist, which is required for Angular
2. Add .yarnrc to avois -W flag during "yarn add ..."

### yarn workspace
````
List/change 'packages' and 'nohoist packages' in package.json 'workspaces'.

yarn install

All packages/libs will be accessible in '/node_modules/@ng-nest-postgre'.
For example, any package can import 'NgxSharedComponenet' from 'ngx-shared' library like:

import { NgxSharedComponenet } from '@ng-nest-postgre/luv-coffee-fe/dist/ngx-shared';

PS: If Angular app is importing library from another Angular app, then it should preserve symlink:
  angular.json:
  -------------------------------------------------------------------------------------
    "luv-coffee-fe": {
      ...
      "architect": {
        "build": {
          ...
            "preserveSymlinks": true
  -------------------------------------------------------------------------------------

  tsconfig.json:
  -------------------------------------------------------------------------------------
  {
    ...
    "compilerOptions": {
      ...
      "paths": {
        "@angular/*": [
          "./node_modules/@angular/*"
        ],
  -------------------------------------------------------------------------------------

  shared.module.ts
  -------------------------------------------------------------------------------------
  import { NgxSharedComponenet } from '@ng-nest-postgre/luv-coffee-fe/dist/ngx-shared';
  -------------------------------------------------------------------------------------

  style.scss
  -------------------------------------------------------------------------------------
  @use '@ng-nest-postgre/luv-coffee-fe/dist/ngx-shared/styles/mixins';
  -------------------------------------------------------------------------------------
````

## commitizen
````
commitizen init cz-conventional-changelog --yarn --dev --exact

git add .
git cz
````

## Husky https://typicode.github.io/husky/get-started.html
npm install --save-dev husky
npx husky init

## Setup Lerna, Husky, and commitizen tutorial
https://www.youtube.com/watch?v=_JAP9gq8hpc
https://lerna.js.org/docs/getting-started


## Docker & Yarn Workspace
````
  Yarn Workspace has only one "yarn.lock" file on the root.
  To run Docker for any workspace project the "yarn.lock" file should be copied to project folder.
````

# REFACTOR TO NEW STARTUP